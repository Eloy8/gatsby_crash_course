import React from "react"
import Layout from "../components/layout"
import { Link } from "gatsby"

const BlogPage = ({data}) => (
  <Layout>
    <h1>Latest Posts</h1>
    {data.allMarkdownRemark.edges.map( post => {
      const { author, title, path, date } = post.node.frontmatter
      const { id } = post.node
      return (
      <div key={id}>
        <h3>{title}</h3>
        <small>Posted by {author} on {date}</small>
        <br />
        <br />
        <Link to={path}>Read more</Link>
        <br />
        <br />
        <hr />
      </div>
      );
    })}
  </Layout>
)

export const pageQuery = graphql`
  	query BlogIndexQuery {
      allMarkdownRemark {
        edges {
          node {
            id
            frontmatter {
              path
              title
              date
              author
            }
          }
        }
      }
    }
`


export default BlogPage
